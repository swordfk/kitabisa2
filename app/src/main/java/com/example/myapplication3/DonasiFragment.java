package com.example.myapplication3;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class DonasiFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_donasi, container, false);

        RecyclerView rckebaikan = v.findViewById(R.id.recyclerview_kebaikan);
        RecyclerView rcpenggalangan = v.findViewById(R.id.recyclerview_penggalangan);

        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rckebaikan.setLayoutManager(horizontalLayoutManager);
        rckebaikan.setAdapter(new AdapterKebaikan(getActivity()));

        LinearLayoutManager horizontalLayoutManager2
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rcpenggalangan.setLayoutManager(horizontalLayoutManager2);
        rcpenggalangan.setAdapter(new AdapterPenggalangan(getActivity()));

        ImageView notif = v.findViewById(R.id.notif);
        notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), NotifikasiAct.class));
            }
        });

        return v;

    }
}