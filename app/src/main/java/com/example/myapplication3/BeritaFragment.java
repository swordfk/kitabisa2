package com.example.myapplication3;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BeritaFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_berita, container, false);

        RecyclerView rcBerita = v.findViewById(R.id.recyclerview_berita);
        rcBerita.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcBerita.setAdapter(new AdapterBerita(getActivity()));

        return v;
    }
}