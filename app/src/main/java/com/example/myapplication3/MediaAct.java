package com.example.myapplication3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

public class MediaAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);

        GridView gridView = findViewById(R.id.gridview);

        // create a object of myBaseAdapter
        MyBaseAdapter baseAdapter = new MyBaseAdapter(this);
        gridView.setAdapter(baseAdapter);

        GridView gridView2 = findViewById(R.id.gridview2);

        // create a object of myBaseAdapter
        MyBaseAdapter baseAdapter2 = new MyBaseAdapter(this);
        gridView2.setAdapter(baseAdapter2);

        ImageView back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}